## docker-compose
To run, enter the following command

```
docker-compose --env-file .env  up
```
## Ports
| app           | port     |
| ------        | ------   |
| nginx, Grafana|  80      |
| gitlab        |  8080    |
| prometheus    |  9090    |
| plantUML      |  9999    |


## Grafana Dashboards

JSON models dashboards are in [grafana/dashboards](https://gitlab.com/guard-devops/administration/-/tree/main/grafana/dashboards?ref_type=heads)

## Gitlab credentials

| Username | Password         |
| ------   | ------           |
|  root    |my-strong-password|

## Grafana credentials

| Username | Password |
| ------   | ------     |
|  admin   |  admin     |

## MySql credentials

Credentials from MySql are located in [.env](https://gitlab.com/guard-devops/administration/-/blob/main/.env?ref_type=heads)
