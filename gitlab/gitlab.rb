external_url 'http://gitlab.test.com'

### Default Theme
gitlab_rails['gitlab_default_theme'] = 2

## GitLab Pages
pages_external_url "http://pages.test.com"
gitlab_pages['enable'] = true
gitlab_pages['status_uri'] = "/@status"
gitlab_pages['log_format'] = "json"
gitlab_pages['gitlab_server'] = 'http://gitlab.test.com' # Defaults to external_url
gitlab_pages['internal_gitlab_server'] = 'http://127.0.0.1:8080' # Defaults to gitlab_server, can be changed to internal load balancer

## PlantUML
gitlab_rails['env'] = { 'PLANTUML_ENCODING' => 'deflate' }

## Nginx
nginx['enable'] = true
nginx['custom_gitlab_server_config'] = "location /-/plantuml/ { \n proxy_cache off; \n    proxy_pass  http://plantuml:9999/; \n}\n"

##  Auth
gitlab_rails['initial_root_password'] = 'my-strong-password'
